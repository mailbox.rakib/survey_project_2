from django.urls import path
from .admin import site
from .views import StartSurvey


urlpatterns = [
    path('admin/', site.urls),
    path('start_survey/<int:survey_id>/', StartSurvey.as_view())
]
