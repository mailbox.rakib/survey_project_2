from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User


class Question(models.Model):
    QUESTION_TYPES = (
        ('text_field', 'Text Field'),
        ('radio_select', 'Radio Select'),
        ('multiple_select', 'Multiple Select'),
    )
    question_text = models.CharField(max_length=200, verbose_name='Question')
    pub_date = models.DateTimeField('date published')
    question_type = models.CharField(choices=QUESTION_TYPES, default='radio_select', max_length=30)

    def __str__(self):
        return self.question_text

    def choices_count(self):
        return self.choices.count()

    choices_count.short_description = 'Number of choices'


class Choice(models.Model):
    question = models.ForeignKey(Question,
                                 on_delete=models.CASCADE,
                                 related_name='choices')
    choice_text = models.CharField(max_length=200)

    def clean(self):
        self._state.delete = True

    def __str__(self):
        return self.choice_text


class Survey(models.Model):
    survey_text = models.CharField(max_length=200, verbose_name='Survey Name')
    questions = models.ManyToManyField(Question, through='SurveyQuestions')
    duration = models.IntegerField(verbose_name='Duration (mins)')

    class Meta:
        permissions = (
            ('participate_in_survey', 'Can participate any survey'),
        )


class SurveyQuestions(models.Model):
    survey = models.ForeignKey(Survey, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)


class UserSurveySession(models.Model):
    survey = models.ForeignKey(Survey, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)


@receiver(post_save, sender=Choice, dispatch_uid="type_check_operation")
def delete_if_text_field(sender, instance, **kwargs):
    question = instance.question
    if question.question_type == 'text_field':
        instance.delete()


@receiver(post_save, sender=Question, dispatch_uid="type_check_action")
def delete_linked_choices(sender, instance, **kwargs):
    choices = instance.choices.all()
    if instance.question_type == 'text_field':
        for choice in choices:
            choice.delete()


