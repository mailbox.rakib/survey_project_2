from django.contrib import admin
from django.utils import timezone
from django.contrib.admin import register
from django.contrib.admin import ModelAdmin as ModelAdmin
from .models import Choice, Question, Survey, SurveyQuestions
from django.utils.html import mark_safe
site = admin.site


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 0
    fields = ['choice_text']

    def get_min_num(self, request, obj=None, **kwargs):
        if request.POST and request.POST['question_type'] != 'text_field':
            return 2
        return super(ChoiceInline, self).get_min_num(request, obj, **kwargs)


@register(Question)
class QuestionAdmin(ModelAdmin):
    icon_name = 'question_answer'
    list_display = ('question_text', 'question_type', 'pub_date')
    search_fields = ['question_text']
    inlines = [ChoiceInline]
    list_filter = ['question_text']

    class Media:
        js = ('survey_app/questions/js/hide_show_choices.js',)

    def get_changeform_initial_data(self, request):
        return {'pub_date': timezone.now()}


class SurveyQuestionInline(admin.TabularInline):
    model = SurveyQuestions
    extra = 0
    min_num = 1

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj=None, **kwargs)
        formset.validate_min = True
        return formset


@register(Survey)
class SurveyAdmin(ModelAdmin):
    list_display = ('survey_text', 'questions_count', 'start_survey')
    inlines = [SurveyQuestionInline]

    def questions_count(self, obj):
        return obj.questions.count()

    questions_count.allow_tags = True
    questions_count.short_description = 'Total Questions'

    def start_survey(self, obj):
        link = f'/start_survey/{obj.id}/'
        return mark_safe(
            f'''
            <a class="nav-link" href="{link}">
					<svg width="16" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M9.95572 15.565C10.5357 16.145 11.4757 16.145 12.0557 15.565L18.5707 9.048C18.7087 8.91034 18.8181 8.74684 18.8928 8.56683C18.9674 8.38683 19.0059 8.19387 19.0059 7.999C19.0059 7.80413 18.9674 7.61117 18.8928 7.43117C18.8181 7.25116 18.7087 7.08766 18.5707 6.95L12.0537 0.435C11.4747 -0.145001 10.5357 -0.145001 9.95572 0.435L10.6557 1.134C10.7486 1.04118 10.8745 0.989035 11.0057 0.989035C11.137 0.989035 11.2629 1.04118 11.3557 1.134L17.8717 7.65C17.9645 7.74283 18.0167 7.86873 18.0167 8C18.0167 8.13127 17.9645 8.25717 17.8717 8.35L11.3557 14.865C11.2629 14.9578 11.137 15.01 11.0057 15.01C10.8744 15.01 10.7486 14.9578 10.6557 14.865L9.95572 15.565Z" fill="black" fill-opacity="0.8"></path>
						<path d="M5.71776 4.49744C5.66602 4.54905 5.62498 4.61036 5.59697 4.67785C5.56896 4.74535 5.55455 4.8177 5.55455 4.89078C5.55455 4.96385 5.56896 5.03621 5.59697 5.1037C5.62498 5.1712 5.66602 5.2325 5.71776 5.28411L8.10332 7.66855L0.55554 7.66855C0.408197 7.66855 0.266889 7.72709 0.162702 7.83127C0.0585154 7.93546 -1.54224e-05 8.07677 -1.54288e-05 8.22411C-1.54353e-05 8.37145 0.0585154 8.51276 0.162702 8.61695C0.266889 8.72113 0.408197 8.77967 0.55554 8.77967L8.10332 8.77967L5.71776 11.1641C5.61344 11.2684 5.55484 11.4099 5.55484 11.5574C5.55484 11.705 5.61344 11.8465 5.71776 11.9508C5.82208 12.0551 5.96357 12.1137 6.11109 12.1137C6.25862 12.1137 6.40011 12.0551 6.50443 11.9508L9.83776 8.61744C9.8895 8.56584 9.93055 8.50453 9.95855 8.43704C9.98656 8.36954 10.001 8.29718 10.001 8.22411C10.001 8.15104 9.98656 8.07868 9.95855 8.01118C9.93055 7.94369 9.8895 7.88238 9.83776 7.83078L6.50443 4.49744C6.45282 4.44571 6.39152 4.40466 6.32402 4.37665C6.25653 4.34864 6.18417 4.33423 6.11109 4.33423C6.03802 4.33423 5.96566 4.34864 5.89817 4.37665C5.83067 4.40466 5.76937 4.44571 5.71776 4.49744Z" fill="black" fill-opacity="0.8"></path>
					</svg>
				</a>
            '''
        )

    start_survey.allow_tags = True
    start_survey.short_description = ''


site.site_header = 'Impel Surveys'
