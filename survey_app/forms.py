from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, HTML, Submit


class QuestionForm(forms.Form):
    question_text = forms.CharField(widget=forms.HiddenInput, disabled=True)
    question_type = forms.CharField(widget=forms.HiddenInput)
    is_last = forms.BooleanField(widget=forms.HiddenInput, required=False)
    is_first = forms.BooleanField(widget=forms.HiddenInput, required=False)

    class Media:
        js = ['admin/js/vendor/jquery/jquery.js',
              'survey_app/js/jquery.plugin.min.js',
              'survey_app/js/jquery.countdown.js',
              ]
        css = {'all': ('survey_app/css/jquery.countdown.css',)}

    def __init__(self, **kwargs):
        question = kwargs['initial'].pop('question')
        question_type = question.question_type
        kwargs['initial']['question_text'] = question.question_text
        kwargs['initial']['question_type'] = question_type
        question_choices = question.choices.all()
        super(QuestionForm, self).__init__(**kwargs)

        choices_tuples = tuple(zip(question_choices, question_choices))

        if question_type == 'radio_select':
            self.fields['answer'] = forms.ChoiceField(choices=choices_tuples,
                                                            widget=forms.RadioSelect,
                                                            label='', required=False)

        elif question_type == 'multiple_select':
            self.fields['answer'] = forms.MultipleChoiceField(choices=choices_tuples,
                                                            widget=forms.CheckboxSelectMultiple,
                                                            label='', required=False)
        else:
            self.fields['answer'] = forms.CharField(widget=forms.TextInput, label='', required=False)

        if kwargs['initial'].get('answer'):
            self.fields['answer'].initial = kwargs['initial'].get('answer')

        self.helper = FormHelper()
        self.helper.form_id = 'id-survey_form'

        layout_list = [HTML(f'<p style="font-weight: 500">{question.question_text}</p>'),*self.fields.keys(),]
        if not kwargs['initial']['is_first']:
            layout_list.append(Submit('save', 'Prev'))
        if not kwargs['initial']['is_last']:
            layout_list.append(Submit('save', 'Next'))
        if kwargs['initial']['is_last']:
            layout_list.append(Submit('save', 'Finish'))

        self.helper.layout = Layout(*layout_list)

