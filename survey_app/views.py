from django.views.generic import FormView
from .models import Survey, UserSurveySession
from .forms import QuestionForm
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.sessions.backends.db import SessionStore
import datetime
from django.contrib.auth.views import redirect_to_login
from django.shortcuts import redirect
from django.contrib import messages


class StartSurvey(UserPassesTestMixin, FormView):
    form_class = QuestionForm
    survey_id = None
    template_name = 'survey_app/start_survey.html'

    def test_func(self):
        return self.request.user.has_perm('survey_app.view_survey') and self.get_duration_left() > 0

    def handle_no_permission(self):
        if self.request.user.has_perm('survey_app.view_survey') and self.get_duration_left() <= 0:
            messages.add_message(self.request, messages.INFO, 'You have completed this survey')
            return redirect('/admin/survey_app/survey/')
        return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        survey = self.get_survey_object()
        context_data.update({'survey_name': survey.survey_text})
        context_data.update({'duration_left': self.get_duration_left()})
        return context_data

    def get_success_url(self):
        survey_id = self.get_survey_id()
        if self.request.POST.get('save') == 'Finish':
            return '/admin/survey_app/survey/'
        return f'/start_survey/{survey_id}/'

    def form_valid(self, form):
        session = self.get_session()
        if 'question_answer' not in session:
            session['question_answer'] = {}
        session['question_answer'].update({
            form.cleaned_data.get('question_text'): form.cleaned_data.get('answer')
        })
        if self.request.POST.get('save') == 'Prev':
            session['current_question_index'] -= 1
        if self.request.POST.get('save') == 'Next':
            session['current_question_index'] += 1
        session.save()
        return super().form_valid(form)

    def get_initial(self):
        current_question_index = self.get_current_question_index()
        question = self.get_current_question(current_question_index)
        question_count = self.get_questions_count()
        initial = super().get_initial()
        is_first = True if current_question_index == 1 else False
        is_last = True if current_question_index == question_count else False
        initial.update({'question': question,
                        'is_first': is_first,
                        'is_last': is_last,
                        'survey_id': self.get_survey_id()})
        answer = self.get_current_question_prev_answer(question_text=question.question_text)
        if answer:
            initial.update({'answer': answer})
        return initial

    def get_duration_left(self):
        session = self.get_session()
        expire_on = datetime.datetime.fromtimestamp(session['expire_on'])
        time_delta = (expire_on - datetime.datetime.now())
        total_seconds = time_delta.total_seconds()
        return total_seconds

    def get_questions_count(self):
        survey = self.get_survey_object()
        return survey.questions.count()

    def get_current_question_index(self):
        session = self.get_session()
        session_dict = session.load()
        if 'current_question_index' in session_dict:
            return session_dict.get('current_question_index')
        else:
            session['current_question_index'] = 1
            session.save()
            return 1

    def get_current_question_prev_answer(self, question_text=''):
        session = self.get_session()
        if 'question_answer' in session:
            if question_text in session['question_answer']:
                return session['question_answer'][question_text]
        return None

    def get_survey_id(self):
        return self.request.resolver_match.kwargs['survey_id']

    def get_survey_object(self):
        return Survey.objects.get(id=self.get_survey_id())

    def get_current_question(self, current_question_index):
        survey = self.get_survey_object()
        return survey.questions.all()[current_question_index-1:
                                      current_question_index].get()

    def get_session_hash(self):
        user_id = self.request.user.id
        survey_id = self.get_survey_id()
        survey_question_session = UserSurveySession.objects.filter(user_id=user_id,
                                                                       survey_id=survey_id).first()
        if not survey_question_session:
            new_session = self.create_session()
            UserSurveySession.objects.create(user_id=user_id, survey_id=survey_id,
                                                 session_id=new_session.session_key)

            session_hash = new_session.session_key
        else:
            session_hash = survey_question_session.session.session_key
        return session_hash

    def create_session(self):
        new_session = SessionStore()
        new_session.create()
        survey = self.get_survey_object()
        new_session.set_expiry(survey.duration * 60)
        now = datetime.datetime.now()
        new_session['expire_on'] = now + datetime.timedelta(minutes=survey.duration)
        new_session['expire_on'] = new_session['expire_on'].timestamp()
        new_session.set_expiry(now + datetime.timedelta(days=365))
        new_session.save()
        return new_session

    def get_session(self):
        session_hash = self.get_session_hash()
        session = SessionStore(session_key=session_hash)
        return session
