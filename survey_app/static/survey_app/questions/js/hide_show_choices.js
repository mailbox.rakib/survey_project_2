window.addEventListener("load", function() {
    (function($) {
        $('#id_question_type').change(function (){
            console.log($(this).val());
            if ($(this).val() == 'text_field'){
                $('#choices-group').hide();
            }
            else{
                $('#choices-group').show();
            }
        });
        if ($('#id_question_type').val() == 'text_field'){
            $('#choices-group').hide();
        }
    })(django.jQuery);
});
